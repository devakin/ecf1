/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package classe.metier;

import exception.metier.personnalise.AttributSocieteInvalide;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;

/**
 *Classe métier prospect qui hérite de la classe mère Societe 
 * @author cda 24
 */
public class Prospect extends Societe{   
    
    
    // Attribut Static----------------------------------------------------------
    
    private static ArrayList<Prospect> listeProspect = new ArrayList<Prospect>();
    
    // Trie par rapport aux raison social et par ordre alphabetique 
    public static Comparator<Prospect> comparateurRaisonSocial = new Comparator<Prospect>(){
        @Override
        public int compare(Prospect p1, Prospect p2){
            return (p1.getRaisonSocialeSociete().compareToIgnoreCase(p2.getRaisonSocialeSociete()));
        }
    };

    
    // Getter et Setter Static--------------------------------------------------
    
    /**
     * Lire listeProspect et faire trier la liste à chaque lecture
     * @return type Arraylist
     */
    public static ArrayList<Prospect> getListeProspect() {
        Collections.sort(listeProspect, comparateurRaisonSocial);
        return listeProspect;
    }

    /**
     * Attribuer une nouvelle Arraylist dans listeProspect
     * @param listeProspect type Arraylist
     */
    public static void setListeProspect(ArrayList<Prospect> listeProspect) {
        Prospect.listeProspect = listeProspect;
    }

    
    // Methode Static-----------------------------------------------------------
    
    public static void initClients() throws AttributSocieteInvalide{
        
        Prospect prospect1 = new Prospect();
        Prospect prospect2 = new Prospect();
        Prospect prospect3 = new Prospect();
        Prospect prospect4 = new Prospect();
        
        prospect1.setIdSociete(Societe.getCompteurIdSociete());
        prospect1.setRaisonSocialeSociete("SIEMENS");
        prospect1.setDomaineSociete(DomaineSociete_Enum.PRIVE);
        prospect1.setNumeroRueSociete(1);
        prospect1.setNomRueSociete("Allee Adrienne Lecouvreur");
        prospect1.setCodePostalSociete("75000");
        prospect1.setVilleSociete("Paris");
        prospect1.setTelephoneSociete("0987654321");
        prospect1.setMailSociete("contact@siemens.com");
        prospect1.setCommentairesSociete("En croissance");
        prospect1.setDateProspectionProspect("10/12/2019");
        prospect1.setProspectInteresseProspect("OUI");
        
        prospect2.setIdSociete(Societe.getCompteurIdSociete());
        prospect2.setRaisonSocialeSociete("LU SAS");
        prospect2.setDomaineSociete(DomaineSociete_Enum.PRIVE);
        prospect2.setNumeroRueSociete(2);
        prospect2.setNomRueSociete("Allee Adrienne Lecouvreur");
        prospect2.setCodePostalSociete("75000");
        prospect2.setVilleSociete("Paris");
        prospect2.setTelephoneSociete("0987654321");
        prospect2.setMailSociete("contact@lusas.com");
        prospect2.setCommentairesSociete("Ouverture nouvelle usine");
        prospect2.setDateProspectionProspect("10/12/2019");
        prospect2.setProspectInteresseProspect("OUI");
        
        prospect3.setIdSociete(Societe.getCompteurIdSociete());
        prospect3.setRaisonSocialeSociete("MAIRIE DE METZ");
        prospect3.setDomaineSociete(DomaineSociete_Enum.PUBLIC);
        prospect3.setNumeroRueSociete(1);
        prospect3.setNomRueSociete("Place dArmes");
        prospect3.setCodePostalSociete("57000");
        prospect3.setVilleSociete("Metz");
        prospect3.setTelephoneSociete("0987654321");
        prospect3.setMailSociete("contact@mairiemetz.fr");
        prospect3.setCommentairesSociete("Appel d'offre bientot");
        prospect3.setDateProspectionProspect("10/12/2019");
        prospect3.setProspectInteresseProspect("OUI");
        
        prospect4.setIdSociete(Societe.getCompteurIdSociete());
        prospect4.setRaisonSocialeSociete("SNCF");
        prospect4.setDomaineSociete(DomaineSociete_Enum.PUBLIC);
        prospect4.setNumeroRueSociete(123);
        prospect4.setNomRueSociete("Place dArmes");
        prospect4.setCodePostalSociete("75002");
        prospect4.setVilleSociete("Paris");
        prospect4.setTelephoneSociete("0987654321");
        prospect4.setMailSociete("contact@sncf.fr");
        prospect4.setCommentairesSociete("A recontacter");
        prospect4.setDateProspectionProspect("10/12/2019");
        prospect4.setProspectInteresseProspect("NON");
        
        getListeProspect().add(prospect1);
        getListeProspect().add(prospect2);
        getListeProspect().add(prospect3);
        getListeProspect().add(prospect4);
       
    }

    
    // Attribut d'instance------------------------------------------------------
    
    private String dateProspectionProspect;     // Date de prospection
    private String prospectInteresseProspect;   // Prospect interessé OUI ou NON
    
    
    // Getter et Setter d'insatance---------------------------------------------

    /**
     * Lire date de prospection
     * @return the dateProspectionProspect type String
     */
    public String getDateProspectionProspect() {
        return dateProspectionProspect;
    }

    /**
     * Ecrire date de prospection
     * @param dateProspectionProspect the dateProspectionProspect to set type String
     * @throws exception.metier.personnalise.AttributSocieteInvalide Erreur si le saisie n'est pas au format jj/mm/aaaa
     */
    public void setDateProspectionProspect(String dateProspectionProspect) throws AttributSocieteInvalide {
        if(!dateProspectionProspect.matches("^([0-2][0-9]|(3)[0-1])(\\/)(((0)[0-9])|((1)[0-2]))(\\/)\\d{4}$")){
            throw new AttributSocieteInvalide("Le date doit etre de format saisie au format jj/mm/aaaa");
        } 
        this.dateProspectionProspect = dateProspectionProspect;
    }

    /**
     * Lire prospect interessé
     * @return the prospectInteresseProspect type String
     */
    public String getProspectInteresseProspect() {
        return prospectInteresseProspect;
    }

    /**
     * Ecrire prospect interessé
     * @param prospectInteresseProspect the prospectInteresseProspect to set type String
     * @throws exception.metier.personnalise.AttributSocieteInvalide
     * Erreur si le saisie n'est pas OUI ou NON
     */
    public void setProspectInteresseProspect(String prospectInteresseProspect) throws AttributSocieteInvalide {
        if(!prospectInteresseProspect.matches("OUI|NON")){
            throw new AttributSocieteInvalide("Le prospect interessé doit etre sélectionné");
        } 
        this.prospectInteresseProspect = prospectInteresseProspect;
    }
    
   
    // Constructeur------------------------------------------------------------- 

    /**
     * Constructeur Prospect sans les attributs
     */
    public Prospect() {
    }
       
    
    // Le toString--------------------------------------------------------------

    /**
     * Affiche uniquement la raison sociale
     * @return type String
     */
    @Override
    public String toString() {
        //On peut aussi utiliser cette ligne pour afficher tous les attributs
        //return super.toString() + "\nDate de prospection: " + dateProspectionProspect + "\nProspect interesse: " + prospectInteresseProspect;
        return this.getRaisonSocialeSociete();
    } 
            
    
}
