/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package ecf1;

import classe.metier.Client;
import classe.metier.Prospect;
import exception.metier.personnalise.AttributSocieteInvalide;
import jframe.EcfJFrame1;
import jframe.EcfJFrame3;

/**
 * Main
 * Initialise la liste client et prospect
 * Lance la premiere fenetre
 * @author cda 24
 */
public class Ecf1 {

    /**
     * @param args the command line arguments
     * @throws exception.metier.personnalise.AttributSocieteInvalide Exception
     */
    public static void main(String[] args) throws AttributSocieteInvalide {
       
        // Initialisation de quelques Clients et Prospect 
      
        Client.initClients();
        Prospect.initClients();
        
        // Lancement Premiere fenetre
        
        EcfJFrame1 ecfJFrame = new EcfJFrame1();
        ecfJFrame.setTitle("Fenetre1");
        // Activer la visibilité de la fenetre 
        ecfJFrame.setVisible(true);  
        
        
       
    }
    
}
