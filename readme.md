# Projet Personnel Gestion de Prospection Commerciale

- Ce programme gère les prospects et les clients d’un agent commerciale
- La partie Front-End à était réalisé avec Swing
- La partie Back-End à était réalisé en Java 8
   

# Prérequis

- Installer le JDK Java 8 ou supérieur 
- Cloner le projet qui se trouve sur gitlab


# Démarrage

- Vous pouvez lancer le projet à partir du fichier "Gestionnaire commercial.jar"


 
